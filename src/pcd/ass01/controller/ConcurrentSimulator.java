package pcd.ass01.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import pcd.ass01.controller.body.BodiesWorker;
import pcd.ass01.model.Body;
import pcd.ass01.model.BodyPair;
import pcd.ass01.model.Boundary;
import pcd.ass01.model.Position;
import pcd.ass01.model.Velocity;
import pcd.ass01.view.SimulationControllable;

public class ConcurrentSimulator implements Simulator {

	private static final int WORKERS_MAX_NUM = Runtime.getRuntime().availableProcessors();
	private static final double DELTA_TIME = 0.1;
	private final int stepsNum;
	private final double bodyRadius;

	private final Random rand = new Random(System.currentTimeMillis());

	private Optional<SimulationControllable> viewer = Optional.empty();
	private final Boundary bounds;
	private final List<Body> bodies = new ArrayList<>();

	private boolean isStopped = false;

	public ConcurrentSimulator(int bodiesNum, int stepsNum, double bodyRadius) {
		this.stepsNum = stepsNum;
		this.bodyRadius = bodyRadius;
		bounds = new Boundary(-1, -1, 1, 1);
		for (int i = 0; i < bodiesNum; i++) {
			bodies.add(spawnNewBody());
		}
	}

	public ConcurrentSimulator(int bodiesNum, int stepsNum, double bodyRadius, SimulationControllable viewer) {
		this(bodiesNum, stepsNum, bodyRadius);
		this.viewer = Optional.of(viewer);
		this.viewer.get().setSimulator(this);
		this.isStopped = true;
	}

	private Body spawnNewBody() {
		final double x = getRandomValue(bounds.getX0(), bounds.getX1());
		final double y = getRandomValue(bounds.getY0(), bounds.getY1());
		final double deltaX = getRandomValue(-1, 1);
		final double speed = getRandomValue(0, 0.05);
		final double xVelocity = deltaX * speed;
		final double yVelocity = Math.sqrt(1 - deltaX * deltaX) * speed;
		return new Body(new Position(x, y), new Velocity(xVelocity, yVelocity), bodyRadius);
	}

	private double getRandomValue(double min, double max) {
		return min + rand.nextDouble() * (max - min);
	}

	@Override
	public long execute() {
		final List<List<Body>> bodyPartitions = partition(bodies, WORKERS_MAX_NUM);
		final List<List<BodyPair>> bodyPairs = partition(createBodyPairList(bodies), WORKERS_MAX_NUM);
		final StateUpdater stateUpdater = new StateUpdater(DELTA_TIME, stepsNum);
		final CyclicBarrier positionBarrier = new CyclicBarrier(bodyPartitions.size());
		final int collisionBarrierParties = bodyPartitions.size() + (viewer.isPresent() ? 1 : 0);
		final CyclicBarrier collisionBarrier = new CyclicBarrier(collisionBarrierParties, stateUpdater);

		for (int i = 0; i < bodyPartitions.size(); i++) {
			final BodiesWorker worker = new BodiesWorker("Worker-" + i, bodyPartitions.get(i), bodyPairs.get(i),
					DELTA_TIME, bounds, positionBarrier, collisionBarrier);
			stateUpdater.addObserver(worker);
			worker.start();
		}

		long start = 0L;
		if (viewer.isPresent()) {
			while (stateUpdater.getCurrentIterations() < stepsNum) {
				synchronized (this) {
					if (isStopped) {
						if (start == 0L)
							viewer.get().handleBegin();
						try {
							wait();
							if (start == 0L)
								start = System.currentTimeMillis();
						} catch (final InterruptedException e) {
							e.printStackTrace();
							System.exit(-1);
						}
					}
				}
				try {
					collisionBarrier.await();
				} catch (InterruptedException | BrokenBarrierException e) {
					e.printStackTrace();
					System.exit(-1);
				}
				viewer.get().display(bodies, stateUpdater.getCurrentVirtualTime(), stateUpdater.getCurrentIterations());
			}
		} else {
			start = System.currentTimeMillis();
		}

		stateUpdater.getObservers().forEach(obs -> {
			try {
				obs.join();
			} catch (final InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		});

		final long executionTime = System.currentTimeMillis() - start;

		if (viewer.isPresent()) {
			viewer.get().handleFinish();
		}

		return executionTime;

	}

	public void handleStart() {
		synchronized (this) {
			notify();
			isStopped = false;
		}
	}

	public void handleStop() {
		synchronized (this) {
			isStopped = true;
		}
	}

	public void handleStep() {
		synchronized (this) {
			notify();
		}
	}

	private <T> List<List<T>> partition(List<T> list, int partitionSize) {
		final List<List<T>> partitions = new ArrayList<>();

		for (int i = 0; i < partitionSize; i++) {
			partitions.add(new ArrayList<>());
		}

		final Iterator<T> iterator = list.iterator();
		while (iterator.hasNext()) {
			for (int i = 0; i < partitionSize; i++) {
				if (!iterator.hasNext()) {
					break;
				}
				partitions.get(i).add(iterator.next());
			}
		}

		return partitions;
	}

	private List<BodyPair> createBodyPairList(List<Body> bodies) {
		final List<BodyPair> bodyPairs = new ArrayList<>();
		for (int i = 0; i < bodies.size() - 1; i++) {
			for (int j = i + 1; j < bodies.size(); j++) {
				bodyPairs.add(new BodyPair(bodies.get(i), bodies.get(j)));
			}
		}
		return bodyPairs;
	}

}
