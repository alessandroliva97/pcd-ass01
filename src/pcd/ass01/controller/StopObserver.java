package pcd.ass01.controller;

public interface StopObserver {

    void notifyStop();

}
