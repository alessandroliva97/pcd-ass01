package pcd.ass01.controller;

import java.util.*;

import pcd.ass01.controller.body.CollisionSolver;
import pcd.ass01.model.Body;
import pcd.ass01.model.Boundary;
import pcd.ass01.model.Position;
import pcd.ass01.model.Velocity;
import pcd.ass01.view.SimulationViewer;

public class SequentialSimulator implements Simulator {

	private Optional<SimulationViewer> viewer = Optional.empty();
	private final int stepsNum;

	/* bodies in the field */
	ArrayList<Body> bodies;

	/* boundary of the field */
	private Boundary bounds;

	public SequentialSimulator(int bodiesNum, int stepsNum, double bodyRadius) {
		this.stepsNum = stepsNum;

		/* initializing boundary and bodies */

		bounds = new Boundary(-1.0, -1.0, 1.0, 1.0);

		/* test with 3 big bodies */
		/*
		 * bodies = new ArrayList<Body>(); bodies.add(new Body(new Position(-0.5,0), new
		 * Velocity(0.005,0), 0.05)); bodies.add(new Body(new Position(0,0.05), new
		 * Velocity(0,0), 0.05)); bodies.add(new Body(new Position(0.07,-0.1), new
		 * Velocity(0,0), 0.05));
		 */

		/* test with 1000 small bodies */

		Random rand = new Random(System.currentTimeMillis());
		bodies = new ArrayList<Body>();
		for (int i = 0; i < bodiesNum; i++) {
			double x = bounds.getX0() + rand.nextDouble() * (bounds.getX1() - bounds.getX0());
			double y = bounds.getX0() + rand.nextDouble() * (bounds.getX1() - bounds.getX0());
			double dx = -1 + rand.nextDouble() * 2;
			double speed = rand.nextDouble() * 0.05;
			Body b = new Body(new Position(x, y), new Velocity(dx * speed, Math.sqrt(1 - dx * dx) * speed), bodyRadius);
			bodies.add(b);
		}
	}

	public SequentialSimulator(int bodiesNum, int stepsNum, double bodyRadius, SimulationViewer viewer) {
		this(bodiesNum, stepsNum, bodyRadius);
		this.viewer = Optional.of(viewer);
	}

	public long execute() {
		long start = System.currentTimeMillis();
		/* init virtual time */

		double vt = 0;
		double dt = 0.1;

		long iter = 0;

		/* simulation loop */

		while (iter < stepsNum) {

			/* compute bodies new pos */

			for (Body b : bodies) {
				b.updatePosition(dt);
			}

			/* check collisions */

			for (int i = 0; i < bodies.size() - 1; i++) {
				Body b1 = bodies.get(i);
				for (int j = i + 1; j < bodies.size(); j++) {
					Body b2 = bodies.get(j);
					if (b1.isCollidingWith(b2)) {
						CollisionSolver.solveCollision(b1, b2);
					}
				}
			}

			/* check boundaries */

			for (Body b : bodies) {
				b.checkAndSolveBoundaryCollision(bounds);
			}

			/* update virtual time */

			vt = vt + dt;
			iter++;

			/* display current stage */

			if (viewer.isPresent()) {
				viewer.get().display(bodies, vt, iter);
			}

		}
		
		return System.currentTimeMillis() - start;
		
	}

}
