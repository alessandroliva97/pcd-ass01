package pcd.ass01.controller.body;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import pcd.ass01.controller.StopObserver;
import pcd.ass01.model.Body;
import pcd.ass01.model.BodyPair;
import pcd.ass01.model.Boundary;

public class BodiesWorker extends Thread implements StopObserver {

	private volatile boolean stopped = false;

	private final List<Body> bodies;
	private final List<BodyPair> pairs;
	private final double deltaTime;
	private final Boundary bounds;
	private final CyclicBarrier positionBarrier;
	private final CyclicBarrier collisionBarrier;

	public BodiesWorker(String name, List<Body> bodies, List<BodyPair> pairs, double deltaTime, Boundary bounds,
			CyclicBarrier positionBarrier, CyclicBarrier collisionBarrier) {
		super(name);
		this.bodies = bodies;
		this.pairs = pairs;
		this.deltaTime = deltaTime;
		this.bounds = bounds;
		this.positionBarrier = positionBarrier;
		this.collisionBarrier = collisionBarrier;
	}

	@Override
	public void run() {
		while (!stopped) {
			bodies.forEach(b -> {
				b.updatePosition(deltaTime);
				b.checkAndSolveBoundaryCollision(bounds);
			});
			try {
				positionBarrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			for (int i = 0; i < pairs.size(); i++) {
				BodyPair bodyPair = pairs.get(i);
				Body body1 = bodyPair.getBody1();
				Body body2 = bodyPair.getBody2();
				if (body1.isCollidingWith(body2)) {
					CollisionSolver.solveCollision(body1, body2);
				}
			}
			try {
				collisionBarrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}

	@Override
	public void notifyStop() {
		stopped = true;
	}

}