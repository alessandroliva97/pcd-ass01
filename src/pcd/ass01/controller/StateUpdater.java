package pcd.ass01.controller;

import java.util.ArrayList;
import java.util.List;

import pcd.ass01.controller.body.BodiesWorker;

public class StateUpdater implements Runnable {
	
	private List<BodiesWorker> observers = new ArrayList<>();
	private double virtualTime = 0;
	private double deltaTime;
	private int nIterations = 0;
	private int nSteps;
	
	public StateUpdater(double deltaTime, int nSteps) {
		super();
		this.deltaTime = deltaTime;
		this.nSteps = nSteps;
	}

	@Override
	public void run() {
		virtualTime += deltaTime;
		nIterations++;
		if (nIterations == nSteps) {
			notifyObservers();
		}
	}
	
	public void addObserver(BodiesWorker obs) {
		observers.add(obs);
	}

	private void notifyObservers() {
		for (StopObserver obs : observers) {
			obs.notifyStop();
		}
	}

	public double getCurrentVirtualTime() {
		return virtualTime;
	}

	public int getCurrentIterations() {
		return nIterations;
	}
	
	public List<BodiesWorker> getObservers() {
		return observers;
	}

}
