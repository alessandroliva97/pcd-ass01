package pcd.ass01.view;

import pcd.ass01.controller.ConcurrentSimulator;

public interface SimulationControllable extends SimulationViewer {

    public void setSimulator(ConcurrentSimulator simulator);

    public void handleBegin();

    public void handleFinish();

}
