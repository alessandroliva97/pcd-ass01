package pcd.ass01.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JPanel;

import pcd.ass01.model.Body;
import pcd.ass01.model.Position;

public class SimulationPanel extends JPanel {

	/**
	 * A unique serial version identifier
	 * 
	 * @see Serializable#serialVersionUID
	 */
	private static final long serialVersionUID = -165761220960106001L;
	private ArrayList<Body> bodies = new ArrayList<Body>();
	private long dx;
	private long dy;

	public SimulationPanel(int w, int h) {
		setSize(w, h);
		dx = w / 2 - 20;
		dy = h / 2 - 20;
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2.clearRect(0, 0, this.getWidth(), this.getHeight());

		bodies.forEach(b -> {
			Position p = b.getPosition();
			double rad = b.getRadius();
			int x0 = (int) (dx + p.getX() * dx);
			int y0 = (int) (dy - p.getY() * dy);
			g2.drawOval(x0, y0, (int) (rad * dx * 2), (int) (rad * dy * 2));
		});
	}

	public void display(ArrayList<Body> bodies) {
		this.bodies = bodies;
		repaint();
	}
}