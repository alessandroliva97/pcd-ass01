package pcd.ass01.view;

import java.util.Collection;

import pcd.ass01.controller.ConcurrentSimulator;
import pcd.ass01.model.Body;

public class SimulationMockupViewerControllable extends Thread implements SimulationControllable {

	private ConcurrentSimulator simulator;
	private volatile boolean isStopped = false;

	public SimulationMockupViewerControllable() {
	}

	@Override
	public void run() {
		while (true) {
			synchronized (this) {
				if (isStopped) break;
			}
		}
		simulator.handleStart();
		// System.out.println("start");
		simulator.handleStop();
		// System.out.println("stop");
		simulator.handleStep();
		// System.out.println("step");
		simulator.handleStep();
		// System.out.println("step");
		simulator.handleStep();
		// System.out.println("step");
		simulator.handleStart();
		// System.out.println("start");
		simulator.handleStop();
		// System.out.println("stop");
		simulator.handleStart();
		// System.out.println("start");
		while (true) {
			synchronized (this) {
				if (isStopped) break;
			}
		}
	}

	@Override
	public void setSimulator(ConcurrentSimulator simulator) {
		this.simulator = simulator;
	}

	@Override
	public void display(Collection<Body> bodies, double virtualTime, long iteration) {
		return;
	}

	@Override
	public void handleBegin() {
		synchronized (this) {
			this.isStopped = true;
		}
	}

	@Override
	public void handleFinish() {
		synchronized (this) {
			this.isStopped = true;
		}
	}

}