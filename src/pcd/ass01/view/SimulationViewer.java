package pcd.ass01.view;

import java.util.Collection;

import pcd.ass01.model.Body;

public interface SimulationViewer {
	
	public void display(Collection<Body> bodies, double virtualTime, long iteration);

}
