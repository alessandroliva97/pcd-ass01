package pcd.ass01.view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.*;

import pcd.ass01.model.Body;

/**
 * Simulation view
 * 
 * @author aricci
 *
 */
public class SimulationViewerPlain extends JFrame implements SimulationViewer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5516015410790143416L;
	private VisualizerPanel panel;
	
	private static final int DIMENSION = 720;

	/**
	 * Creates a view of the specified size (in pixels)
	 * 
	 * @param w
	 * @param h
	 */
	public SimulationViewerPlain() {
		setTitle("Bodies Simulation");
		setSize(DIMENSION, DIMENSION);
		setResizable(false);
		panel = new VisualizerPanel(DIMENSION, DIMENSION);
		getContentPane().add(panel);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}

			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		setVisible(true);
	}

	public void display(Collection<Body> bodies, double virtualTime, long iteration) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				panel.display(new ArrayList<Body>(bodies), virtualTime, iteration);
			});
		} catch (Exception ex) {
		}
	}

}
