package pcd.ass01.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import pcd.ass01.controller.ConcurrentSimulator;
import pcd.ass01.model.Body;

public class SimulationViewerControllable extends JFrame implements SimulationControllable {

	/**
	 * A unique serial version identifier
	 * 
	 * @see Serializable#serialVersionUID
	 */
	private static final long serialVersionUID = 943153832894696460L;
	private static final int DIMENSION = 720;
	private static final int INFO_DIMENSION = 35;

	private final JButton start;
	private final JButton stop;
	private final JButton step;
	private final JPanel buttonPanel;
	private final SimulationPanel simulationPanel;
	private final JPanel infoPanel;
	private ConcurrentSimulator simulator;

	private final JLabel body;
	private final JLabel vt;
	private final JLabel iter;

	public SimulationViewerControllable() {
		setTitle("Bodies simulation");
		setResizable(false);
		start = new JButton("Start");
		start.setEnabled(false);
		stop = new JButton("Stop");
		stop.setEnabled(false);
		step = new JButton("Step");
		step.setEnabled(false);
		start.addActionListener(e -> {
			start.setEnabled(false);
			stop.setEnabled(true);
			step.setEnabled(false);
			simulator.handleStart();
		});
		stop.addActionListener(e -> {
			start.setEnabled(true);
			stop.setEnabled(false);
			step.setEnabled(true);
			simulator.handleStop();
		});
		step.addActionListener(e -> {
			simulator.handleStep();
		});
		buttonPanel = new JPanel();
		buttonPanel.add(start, BorderLayout.WEST);
		buttonPanel.add(stop, BorderLayout.CENTER);
		buttonPanel.add(step, BorderLayout.EAST);
		simulationPanel = new SimulationPanel(DIMENSION, DIMENSION);
		body = new JLabel("Bodies: 0");
		vt = new JLabel("Virtual Time: 0");
		iter = new JLabel("Iteration: 0");
		infoPanel = new JPanel();
		infoPanel.add(body, BorderLayout.WEST);
		infoPanel.add(vt, BorderLayout.CENTER);
		infoPanel.add(iter, BorderLayout.EAST);
		buttonPanel.setPreferredSize(new Dimension(DIMENSION, INFO_DIMENSION));
		simulationPanel.setPreferredSize(new Dimension(DIMENSION, DIMENSION));
		infoPanel.setPreferredSize(new Dimension(DIMENSION, INFO_DIMENSION));
		getContentPane().add(buttonPanel, BorderLayout.NORTH);
		getContentPane().add(simulationPanel, BorderLayout.CENTER);
		getContentPane().add(infoPanel, BorderLayout.SOUTH);
		setSize(DIMENSION, INFO_DIMENSION + simulationPanel.getHeight() + INFO_DIMENSION);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}

			@Override
			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		setVisible(true);
	}

	@Override
	public void display(Collection<Body> bodies, double virtualTime, long iteration) {

		try {
			SwingUtilities.invokeAndWait(() -> {
				simulationPanel.display(new ArrayList<>(bodies));
				body.setText("Bodies: " + bodies.size());
				vt.setText("Virtual Time: " + String.format("%.2f", virtualTime));
				iter.setText("Iteration: " + iteration);
			});
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void setSimulator(ConcurrentSimulator simulator) {
		this.simulator = simulator;
	}

	@Override
	public void handleBegin() {
		start.setEnabled(true);
		stop.setEnabled(false);
		step.setEnabled(true);
	}

	@Override
	public void handleFinish() {
		for (final Component c : buttonPanel.getComponents()) {
			c.setEnabled(false);
		}
	}

}
