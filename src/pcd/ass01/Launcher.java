package pcd.ass01;

import pcd.ass01.controller.ConcurrentSimulator;
import pcd.ass01.controller.SequentialSimulator;
import pcd.ass01.controller.Simulator;
import pcd.ass01.view.SimulationMockupViewerControllable;
import pcd.ass01.view.SimulationViewerControllable;
import pcd.ass01.view.SimulationViewerPlain;

public class Launcher {

	private static final boolean USE_CONCURRENCY = true;
	private static final boolean USE_VIEW = true;
	private static final boolean MOCKUP_VIEWER = false;

	private static final int BODIES_NUM = 1000;
	private static final int STEPS_NUM = 1000;
	private static final double BODY_RADIUS = 0.01;

	public static void main(String[] args) {

		Simulator simulator;

		if (USE_CONCURRENCY) {
			if (USE_VIEW) {
				if (MOCKUP_VIEWER) {
					final SimulationMockupViewerControllable mockupViewer = new SimulationMockupViewerControllable();
					simulator = new ConcurrentSimulator(BODIES_NUM, STEPS_NUM, BODY_RADIUS, mockupViewer);
					mockupViewer.start();
				} else {
					simulator = new ConcurrentSimulator(BODIES_NUM, STEPS_NUM, BODY_RADIUS,
							new SimulationViewerControllable());
				}
			} else {
				simulator = new ConcurrentSimulator(BODIES_NUM, STEPS_NUM, BODY_RADIUS);
			}
		} else {
			if (USE_VIEW) {
				simulator = new SequentialSimulator(BODIES_NUM, STEPS_NUM, BODY_RADIUS, new SimulationViewerPlain());
			} else {
				simulator = new SequentialSimulator(BODIES_NUM, STEPS_NUM, BODY_RADIUS);
			}
		}

		System.out.println("Computing time: " + simulator.execute() + "ms");

	}
}