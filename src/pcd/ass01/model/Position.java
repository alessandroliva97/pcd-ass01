package pcd.ass01.model;

public class Position {
	
	private double x;
	private double y;

    public Position(double x, double y) {
        update(x, y);
    }

    public void update(double x, double y) {
    	this.x = x;
    	this.y = y;
    }
    
    public double getX() {
    	return x;
    }

    public double getY() {
    	return y;
    }

}
