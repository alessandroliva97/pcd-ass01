package pcd.ass01.model;

public class Velocity {
	
	public double x;
	public double y;

    public Velocity(double x, double y) {
        update(x, y);
    }

    public void update(double x, double y) {
    	this.x = x;
    	this.y = y;
    }
    
    public double getX() {
    	return x;
    }

    public double getY() {
    	return y;
    }
    
    public double getModule() {
    	return x * x + y * y;
    }

}
