package pcd.ass01.model;

public class BodyPair {
	
	private final Body body1;
	private final Body body2;
	
	public BodyPair(Body body1, Body body2) {
		this.body1 = body1;
		this.body2 = body2;
	}
	
	public Body getBody1() {
		return body1;
	}
	
	public Body getBody2() {
		return body2;
	}

}
