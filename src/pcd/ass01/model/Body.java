package pcd.ass01.model;

/*
 * This class represents a body, moving in the field.
 * 
 */
public class Body {

	private Position pos;
	private Velocity vel;
	private double radius;

	public Body(Position pos, Velocity vel, double radius) {
		this.pos = pos;
		this.vel = vel;
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public Position getPosition() {
		return pos;
	}

	public Velocity getVelocity() {
		return vel;
	}

	/**
	 * Update the position, according to current velocity
	 * 
	 * @param dt time elapsed
	 */
	public void updatePosition(double dt) {
		double newPosX = pos.getX() + vel.getX() * dt;
		double newPosY = pos.getY() + vel.getY() * dt;
		pos.update(newPosX, newPosY);
	}

	/**
	 * Change the velocity
	 * 
	 * @param vx
	 * @param vy
	 */
	public void updateVelocity(double vx, double vy) {
		vel.update(vx, vy);
	}

	/**
	 * Computes the distance from the specified body
	 * 
	 * @param b
	 * @return
	 */
	public double getDistance(Body b) {
		double dx = pos.getX() - b.getPosition().getX();
		double dy = pos.getY() - b.getPosition().getY();
		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Check if there is collision with the specified body
	 * 
	 * @param b
	 * @return
	 */
	public boolean isCollidingWith(Body b) {
		double distance = getDistance(b);
		return distance < radius + b.getRadius();
	}

	/**
	 * Check if there collisions with the boundaty and update the position and
	 * velocity accordingly
	 * 
	 * @param bounds
	 */
	public void checkAndSolveBoundaryCollision(Boundary bounds) {
		double x = pos.getX();
		double y = pos.getY();
		if (x > bounds.getX1()) {
			pos.update(bounds.getX1(), pos.getY());
			vel.update(-vel.getX(), vel.getY());
		} else if (x < bounds.getX0()) {
			pos.update(bounds.getX0(), pos.getY());
			vel.update(-vel.getX(), vel.getY());
		} else if (y > bounds.getY1()) {
			pos.update(pos.getX(), bounds.getY1());
			vel.update(vel.getX(), -vel.getY());
		} else if (y < bounds.getY0()) {
			pos.update(pos.getX(), bounds.getY0());
			vel.update(vel.getX(), -vel.getY());
		}
	}

}
